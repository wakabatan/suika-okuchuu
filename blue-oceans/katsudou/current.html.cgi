#!/usr/bin/perl

#(undef, undef, undef, undef, $m, $y) = localtime;  $m++;
my $m = 4;
my $y = 2002;

$y += 1900 if $y < 1000;
$m = sprintf('%02d', $m);

my $myuri = 'http://'.$main::ENV{SERVER_NAME}.':'.$main::ENV{SERVER_PORT}.
            '/okuchuu/blue-oceans/';
my $localpart;

if ($main::ENV{QUERY_STRING} eq 'year') {
  $localpart = $y.'/';
} elsif ($main::ENV{QUERY_STRING} eq 'theme') {
  $y = 2001;
  $localpart = $y.'/theme';
} elsif ($main::ENV{QUERY_STRING} eq 'taikai') {
  $y = 2001;
  $localpart = $y.'/taikai';
} elsif ($main::ENV{QUERY_STRING} eq 'part') {
  $y = 2001;
  $localpart = $y.'/part/latest';
} elsif ($main::ENV{QUERY_STRING} eq 'newmember') {
  $y = 2001;
  $localpart = $y.'/newmember';
} else {
  $localpart = $y.'/'.$m;
}

print "Location: ${myuri}${localpart}\n\n";
