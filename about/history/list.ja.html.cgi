#!/usr/bin/perl

use Suika::CGI;
use H2H;

print jcode::jis(<<EOH, 'euc');
Content-Type: text/html; charset="iso-2022-jp"
Content-Style-Type: text/css

<html xml:lang="ja" xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>おくちゅ。の歩み</title>
<?xml-stylesheet href="/s/simpledoc"?>
<link rel="stylesheet" href="/s/simpledoc" />
<link rev="made" href="mailto:wakaba\@suika.fam.cx" />
<link rel="copyright" href="/c/" />
<meta name="author" content="奥田中学校, 若葉" />
<meta name="keywords" content="奥中, 奥田中学校, 富山市立奥田中学校, おくちゅ。, 歴史, 年表, History, 出来事, できごと, 流れ, 歩み, 伝統, 行事, 一覧, 実績" />
</head>
<body>
<h1>おくちゅ。の歩み</h1>

EOH

opendir DIR, '.';
  my @FILELIST = (grep(/.*\.(?:hnf|h2h)$/, readdir(DIR)));
close DIR;

  my %boptions = (
    directory => '/home/wakaba/lib/H2H/V100/Theme/', theme => 'Glossary',
    theme09_directory => '/home/wakaba/lib/H2H/V090/',
    theme09 => 'glossary',
    title => 'おくちゅ。の歩み',
    keyword => '奥中, 奥田中学校, 富山市立奥田中学校, おくちゅ。, 歴史, 年表, History, 出来事, できごと, 流れ, 歩み, 伝統, 行事, 一覧, 実績',
    description => 'おくちゅ。の過去から現在まで。',
    version => 'H2H/1.0',
  );

for my $fn (@FILELIST) {
  my %options = (%boptions);
  $fn =~ tr/\x0D\x0A//d;
  $fn =~ tr/\x0D\x0A//d;
  if ($fn =~ /^(.+)\.(?:hnf|h2h)$/) {$options{prefix} = $1}
  $options{noheader} = 1; $options{nofooter} = 1;
  open HNF, $fn;
    print jcode::jis(H2H->toHTML(\%options, <HNF>), 'euc');
  close HNF;
}

print jcode::jis(<<EOH, 'euc');

<address>
[<a href="/" title="このサーバーの首頁">/</a>
<a href="/map" title="このサーバーの案内">地図</a>
<a href="/search/" title="このサーバーの検索">検索</a>]
[<a href="../../">伝説のおくちゅ。</a>
<a href="../">おくちゅ。伝説</a>]
</address>
<!--
<div class="update">
<a href="http://validator.w3.org/check/referer" xml:lang="en"><img
        src="http://www.w3.org/Icons/valid-xhtml11" id="w3c-html"
        alt="Valid XHTML 1.1!" style="height: 31px; width: 88px" /></a>
<a href="http://jigsaw.w3.org/css-validator/validator?uri=http://suika.fam.cx/okuchuu/" xml:lang="en">
  <img style="width: 88px; height: 31px" id="w3c-css"
       src="http://jigsaw.w3.org/css-validator/images/vcss" 
       alt="Valid CSS!" /></a></div>
-->
</body></html>
EOH


1;
