#!/usr/bin/perl
use strict;
use warnings;
use Path::Class;
use lib file (__FILE__)->dir->parent->subdir
    ('modules', 'h2h', 'implementation', 'classic')->stringify;

require 'jcode.pl';
use H2H;

my $file = $main::ENV{PATH_TRANSLATED};
die unless -e $file;
my $basepath;
if ($file =~ m#^([\x00-\xFF]+?/)([^/]+)$#) {
  ($basepath, $file) = ($1, $2);
}

print STDOUT "Content-Type: text/html; charset=iso-2022-jp\n\n";

my $output;

  my %boptions = (
    file	=> $file,
    pathtofile	=> $basepath,
    directory	=> '/home/wakaba/public_html/bin/H2H/V100/Theme/', # theme => 'Default',
    theme09_directory	=> '/home/wakaba/public_html/bin/H2H/V090/',
    theme09	=> 'suika',
    prefix	=> 'a',
    title	=> 'ほげる。',
    keyword	=> '文書, 文章, 西瓜, 冬様, 進, おくちゅ。',
    description =>	'なんか文章です。中身は見てのお楽しみ。',
  );
  if(-e $basepath.'.title') {open T,$basepath.'.title';$boptions{title}='';
                              while(<T>) {$boptions{title} .= $_} close T}
  if(-e $basepath.'.header'){$boptions{headerfile} = $basepath.'.header'}
  if(-e $basepath.'.footer'){$boptions{footerfile} = $basepath.'.footer'}
  if (-e $basepath.'.rc')   {require $basepath.'.rc';
                             &H2H::RC::init(\%boptions)}
  my %options = (%boptions);
  #if ($file =~ /^(.+)\.(?:hnf|h2h)$/) {$options{prefix} = $1}
  $options{noheader} = 1; $options{nofooter} = 1;
  open HNF, $basepath.$file;
    $output .= H2H->toHTML(\%options, <HNF>);
  close HNF;

  $boptions{version} = 'H2H/1.0';
  $output = H2H->header(\%boptions).$output.
            H2H->footer(\%boptions);
  jcode::convert(\$output, 'jis', 'euc');
    print  $output;


1;
